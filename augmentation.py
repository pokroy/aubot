import os
import random
from scipy import ndarray

# image processing library
import skimage as sk
from skimage import transform
from skimage import util
from skimage import io

def horizontal_flip(image_array: ndarray):
    # horizontal flip doesn't need skimage, it's easy as flipping the image array of pixels !
    return image_array[:, ::-1]

def flip_from_file(in_path, out_path):
    image_to_transform = sk.io.imread(in_path)
    transformed_image = horizontal_flip(image_to_transform)
    io.imsave(out_path, transformed_image)