from extract_embeddings import extract_embeddings
from train_model import train_model
from recognize import recognize
import time
from threading import Thread
from threading import Lock
import telebot
import os
from augmentation import flip_from_file
import json


class TaskHandler:
            def __init__(self):
                self.task_queue = [] #ЭТО сохранять
                self.send_list = {} #ЭТО сохранять
            def extractEmbeddings(self):
                self.task_queue.append('e')
            def recognizeFaces(self, img_path, sender):
                self.task_queue.append(['r', img_path, sender])
            def doTasks(self):
                while(True):
                    if(len(self.task_queue) >= 1):
                        task = self.task_queue.pop(0)
                        if(task[0] == 'e'):
                            print("[TaskHandler] Updating model abd embedings")
                            extract_embeddings()
                            train_model()
                        elif(task[0] == 'r'):
                            for id in set(recognize(task[1])):
                                print("[TaskHandler] Recognized {}, image {}, from {}.".format(id, task[1], task[2]))
                                if(id != "unknown") and (id != str(task[2])):
                                    if id in self.send_list.keys():
                                        self.send_list[id].append(task[1])
                                    else:
                                        self.send_list[id] = [task[1]]
                    elif(len(self.send_list) >= 1):
                        user_id = list(self.send_list.keys())[0]
                        photos_to_send = self.send_list.pop(user_id)
                        print("[TaskHandler] Sending to {} images: {}".format(user_id, photos_to_send))
                        for p in photos_to_send:
                            with open(p, 'rb') as new_file:
                                bot.send_photo(user_id, new_file.read())
                    else:
                        #Сохранять файл
                        with open('data.json', 'w') as fp:
                            lis = [task_handler.task_queue, task_handler.send_list, list(new_user_queue), users, loaders]
                            json.dump(lis, fp)
                        time.sleep(1)


while(True):
    try:
        lock = Lock()
        
        new_user_queue = set() #ЭТО сохранять
        users = {} #ЭТО сохранять
        loaders = {} #ЭТО сохранять
        task_handler = TaskHandler()
        
        fp = open('data.json', 'r')
    
        task_handler.task_queue, task_handler.send_list, new_user_queue, users, loaders = json.load(fp)
        new_user_queue = set(map(str, new_user_queue))
        fp.close()
        
        print("[Initialization] new users: {}, users: {}, loaders: {}".format(list(new_user_queue), users, loaders))
    
        bot = telebot.TeleBot('843631323:AAGegOfnRX6oQBKFzpHVjce76B9DoVXQIg4')
        
        

        
        
        task_executor = Thread(target=task_handler.doTasks)
        task_executor.start()
        
        @bot.message_handler(commands=['start'])
        def start(message):
            user = str(message.chat.id)
            bot.send_message(user, "Напишите /adduser, если вы хотите подписаться на отправку ваших фото.")
        
        
        
        @bot.message_handler(commands=['adduser'])
        def new_user(message):
            user = str(message.chat.id)
            if(user not in new_user_queue):
                bot.send_message(user, "Отправьте 10 фото с вашим фото, или используйте /stop , если хотите отправить меньше фото.")
                new_user_queue.add(user)
                users[user] = 0
                
            
        @bot.message_handler(commands=['stop'])
        def stop(message):
            user = str(message.chat.id)
            if(user in new_user_queue):
                new_user_queue.remove(user)
                bot.send_message(user, "{} фото успешно добавлены.".format(users[user]))
                users[user] = 0
                task_handler.extractEmbeddings()
            
        @bot.message_handler(content_types=['photo'])
        def photo(message):
            lock.acquire()
            try:
                user = str(message.chat.id)
                if user in new_user_queue:
                    users[user] += 1
            
                    file_id = message.photo[-1].file_id
                    path = bot.get_file(file_id)
                    downloaded_file = bot.download_file(path.file_path)
            
                    save_path = "dataset//"+str(user)
                    if not os.path.exists(save_path):
                        os.mkdir(save_path)
                    with open(save_path+"//{}.jpg".format(users[user]), 'wb') as new_file:
                        new_file.write(downloaded_file)
                    flip_from_file(save_path+"//{}.jpg".format(users[user]), save_path+"//{}_a.jpg".format(users[user]))
                    if users[user] == 10:
                        bot.send_message(user, "10 фото успешно добавлены.")
                        if(user in new_user_queue):
                            new_user_queue.remove(user)
                        users[user] = 0
                        task_handler.extractEmbeddings()
                else:
                    if(user in loaders.keys()):
                        loaders[user] += 1
                    else:
                        loaders[user] = 1
                    
                    file_id = message.photo[-1].file_id
                    path = bot.get_file(file_id)
                    downloaded_file = bot.download_file(path.file_path)
            
                    save_path = "images//{}_{}.jpg".format(user, str(loaders[user]))
                    with open(save_path, 'wb') as new_file:
                        new_file.write(downloaded_file)
                    
                    task_handler.recognizeFaces(save_path, user)
            finally:
                lock.release()
                
        bot.polling(timeout=120)
    except:
        #pass #Тут сохранять
        with open('data.json', 'w') as fp:
            lis = [task_handler.task_queue, task_handler.send_list, list(new_user_queue), users, loaders]
            json.dump(lis, fp)
